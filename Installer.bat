@echo off
echo Press any key to begin installing TheNameIsPoy AutoHotKey scripts.
pause>nul
cls
echo Installing AutoHotKey scripts.
taskkill /f /im BlackScreen.exe>nul
taskkill /f /im DesktopSwitcher.exe>nul	
taskkill /f /im Hibernate.exe>nul
taskkill /f /im MinimizeWindow.exe>nul
taskkill /f /im PSMenu.exe>nul
taskkill /f /im Screenshot.exe>nul
rmdir /s /q C:\TheNameIsPoy_AHK>nul
rmdir /s /q C:\WillyPP_AHK>nul
mkdir "C:\TheNameIsPoy_AHK"
cd C:\TheNameIsPoy_AHK
mkdir scripts
powershell -Command "Invoke-WebRequest https://github.com/thenameispoy/ahk/raw/master/RunAll.bat -OutFile RunAll.bat"
cd scripts
powershell -Command "Invoke-WebRequest https://github.com/thenameispoy/ahk/raw/master/BlackScreen.exe -OutFile BlackScreen.exe"
powershell -Command "Invoke-WebRequest https://github.com/thenameispoy/ahk/raw/master/DesktopSwitcher.exe -OutFile DesktopSwitcher.exe"
powershell -Command "Invoke-WebRequest https://github.com/thenameispoy/ahk/raw/master/Hibernate.exe -OutFile Hibernate.exe"
powershell -Command "Invoke-WebRequest https://github.com/thenameispoy/ahk/raw/master/MinimizeWindow.exe -OutFile MinimizeWindow.exe"
powershell -Command "Invoke-WebRequest https://github.com/thenameispoy/ahk/raw/master/PSMenu.exe -OutFile PSMenu.exe"
powershell -Command "Invoke-WebRequest https://github.com/thenameispoy/ahk/raw/master/Screenshot.exe -OutFile Screenshot.exe"
cd ..
powershell "$s=(New-Object -COM WScript.Shell).CreateShortcut('C:\TheNameIsPoy_AHK\RunAllTheNameIsPoyAHKScripts.lnk');$s.TargetPath='C:\TheNameIsPoy_AHK\RunAll.bat';$s.Save()"
xcopy /s /y "C:\TheNameIsPoy_AHK\RunAllTheNameIsPoyAHKScripts.lnk" "%appdata%\Microsoft\Windows\Start Menu\Programs\Startup"
start RunAll.bat
cls
echo Scripts successfully installed.
echo Press any key to exit.
pause>nul
del "%~f0" && exit






